## JSciC have moved into visnow.org community!

Now, you can find it under visnow.org GitLab:
https://gitlab.com/visnow.org/JSciC 


## Acknowledgements
The following work was implemented at the Interdisciplinary Centre for Mathematical 
and Computational Modelling (ICM), University of Warsaw, Poland, as a part of the 
OCEAN (Open Centre for Data and its Analysis) project, co-funded by the National 
Centre for Research and Development within Innovative Economy Programme (POIG). 

![OCEAN EU footer](stopka_ENG_100.png)

